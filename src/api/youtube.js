import axios from 'axios'

const KEY = 'AIzaSyC7sd99KzH73_CnfCUflI2P29BYuuhjnH4'

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params:{
        part: 'snippet',
        type: 'video',
        maxResults: 5,
        key: KEY
    }
})